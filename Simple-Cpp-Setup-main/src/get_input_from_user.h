#pragma once
#include <iostream>

template<typename T>
T get_input_from_user()
{
    T input;
    while (!(std::cin >> input)) {
        std::cin.clear();
        std::cin.sync();
        std::cout << "Invalid input.\n";
    }

    return input;
}