
#include <iostream>
#include "get_input_from_user.h"
#include "message.h"
#include "noughts_and_cross.h"
#include "play_guess_the_number.h"
#include "play_hangman.h"

int main()
{
    bool quit = false;
    while (!quit) {
        show_menu();
        const auto capture = get_input_from_user<char>();

        switch (capture) {
        case '1':
            play_guess_the_number();
            break;
        case '2':
            play_hangman();
            break;
        case '3':
            play_noughts_and_cross();
            break;
        case 'q':
            quit = true;
            break;
        default:
            std::cout << "It's not a command!\n";
            break;
        }
    }

    return 0;
}
