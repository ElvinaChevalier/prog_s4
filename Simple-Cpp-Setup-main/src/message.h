#pragma once
#include <iostream>

inline void win_message(int playeur = 2)
{
    if (playeur == 0)
        std::cout << "PLAYEUR 1 WIN" << std::endl;
    else if (playeur == 1)
        std::cout << "PLAYEUR 2 WIN" << std::endl;
    else
        std::cout << "YOU WIN" << std::endl;
}

inline void loose_message()
{
    std::cout << "YOU LOOSE" << std::endl;
}

inline void show_menu()
{
    std::cout << "What do you want to do ? \n 1 : Play Guess a number \n 2 : Play Hangman\n 3 : Play Noughts and cross\n q : Quit \n " << std::endl;
}