#include "play_hangman.h"
#include <array>
#include <iostream>
#include "get_input_from_user.h"
#include "message.h"
#include "random.h"
// ov#include "time.h"

char get_letter_from_user()
{
    char letter;

    while (!(std::cin >> letter) || (int)letter < 65) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "It is not a letter.\n";
    }

    return letter;
}

const char* pick_a_random_word()
{
    static constexpr std::array words = {
        "puet",
        "watermelon",
        "chèvre",
        "goat",
        "biquette",
        "cornebique",
        "beer",
        "chouette",
        "coucous",
        "jeambono",
        "     ",
    };

    return words[rand<int>(0, words.size() - 1)];
}

bool player_is_alive(int nb_lives)
{
    if (nb_lives == 0) {
        return 0;
    }

    return 1;
}

bool player_win(std::string& word_to_guess, std::string& word_user)
{
    bool win = false;
    if (word_to_guess == word_user) {
        win = !win;
    }
    return win;
}

void guess_a_letter_message()
{
    std::cout << "The letter is in the word !" << std::endl;
}

void remove_one_life(int& nb_lives)
{
    nb_lives--;
}

void show_numbers_of_lives(int nb_lives)
{
    std::cout << "Tu as " << nb_lives << " vies" << std::endl;
}

void show_word_user(std::string word_user)
{
    std::cout << word_user << std::endl;
    std::cout << "pick a letter" << std::endl;
}

std::string create_word_to_guess_without_letters(std::string word_to_guess)
{
    std::string word_user = word_to_guess;
    for (unsigned int i = 0; i < word_to_guess.length(); i++) {
        word_user[i] = '_';
    }
    return word_user;
}

bool check_the_guess(std::string word_to_guess, std::string& word_user, char letter_user)
{
    bool letter_found = false;

    for (unsigned int i = 0; i < word_to_guess.length(); i++) {
        if (letter_user == word_to_guess[i]) {
            word_user[i] = word_to_guess[i];
            letter_found = true;
        }
    }

    return letter_found;
}

void play_hangman()
{
    // srand(time(0));
    std::string word_to_guess = pick_a_random_word();
    int         nb_lives      = 8;
    std::string word_user     = create_word_to_guess_without_letters(word_to_guess);

    while (player_is_alive(nb_lives) && !player_win(word_to_guess, word_user)) {
        show_numbers_of_lives(nb_lives);
        show_word_user(word_user);
        const auto letter_user = get_input_from_user<char>();

        if (check_the_guess(word_to_guess, word_user, letter_user))
            guess_a_letter_message();
        else
            remove_one_life(nb_lives);
    }

    if (player_is_alive(nb_lives)) {
        win_message();
    }
    else {
        loose_message();
    }
}