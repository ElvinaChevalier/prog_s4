#include "noughts_and_cross.h"
#include <p6/p6.h>
#include <iostream>
#include "message.h"

void Draw_square(p6::Context& ctx, float TopLeftCorner_x, float TopLeftCorner_y, float radius)
{
    ctx.square(p6::TopLeftCorner{TopLeftCorner_x, TopLeftCorner_y},
               p6::Radius{radius});
}

void Draw_Grid(p6::Context& ctx)
{
    ctx.background(p6::NamedColor::YellowOrange);
    float step = 2.f / 3.f;
    for (float i = -1; i < 1; i += step) {
        for (float j = 1; j > -1; j -= step) {
            Draw_square(ctx, i, j, 1 - step);
        }
    }
}

void Draw_one_token(p6::Context& ctx, const char* path, float x, float y, float radius)
{
    const auto image = p6::load_image(path);
    ctx.image(image, p6::Center{x, y},
              p6::RadiusY{radius});
}

void Draw_tokens(p6::Context& ctx, std::vector<int> tableau_de_tokens)
{
    float pos_x = -0.67;
    float pos_y = 0.67;
    for (int n = 0; n < 3; n++) {
        pos_x = -0.67;
        for (int i = 0 + 3 * n; i < 3 + 3 * n; i++) {
            if (tableau_de_tokens[i] == 0)
                Draw_one_token(ctx, "img/potato.png", pos_x, pos_y, 0.2);
            else if (tableau_de_tokens[i] == 1)
                Draw_one_token(ctx, "img/raclette.png", pos_x, pos_y, 0.35);
            pos_x += 0.67;
        }
        pos_y -= 0.67;
    }
}

int map_to_0_2(float x)
{
    float range        = p6::map(x, -1.f, 1.f, 0.f, 3.f);
    int   cell_clicked = (int)range;
    return cell_clicked;
}

int cell_clicked(float x, float y)
{
    int abscisse = map_to_0_2(x);
    int ordonnee = map_to_0_2(y);

    if (abscisse == 0) {
        if (ordonnee == 0)
            return 6;
        if (ordonnee == 1)
            return 3;
        if (ordonnee == 2)
            return 0;
    }
    else if (abscisse == 1) {
        if (ordonnee == 0)
            return 7;
        if (ordonnee == 1)
            return 4;
        if (ordonnee == 2)
            return 1;
    }
    else if (abscisse == 2) {
        if (ordonnee == 0)
            return 8;
        if (ordonnee == 1)
            return 5;
        if (ordonnee == 2)
            return 2;
    }

    return 12;
}

void win(p6::Context& ctx, int playeur, std::vector<int> tableau_de_tokens)
{
    int win = 2;
    if (tableau_de_tokens[0] == tableau_de_tokens[1] && tableau_de_tokens[1] == tableau_de_tokens[2] && tableau_de_tokens[1] != 2)
        win = playeur;
    else if (tableau_de_tokens[3] == tableau_de_tokens[4] && tableau_de_tokens[4] == tableau_de_tokens[5] && tableau_de_tokens[5] != 2)
        win = playeur;
    else if (tableau_de_tokens[6] == tableau_de_tokens[7] && tableau_de_tokens[7] == tableau_de_tokens[8] && tableau_de_tokens[8] != 2)
        win = playeur;
    else if (tableau_de_tokens[0] == tableau_de_tokens[3] && tableau_de_tokens[3] == tableau_de_tokens[6] && tableau_de_tokens[6] != 2)
        win = playeur;
    else if (tableau_de_tokens[3] == tableau_de_tokens[4] && tableau_de_tokens[4] == tableau_de_tokens[6] && tableau_de_tokens[6] != 2)
        win = playeur;
    else if (tableau_de_tokens[1] == tableau_de_tokens[4] && tableau_de_tokens[4] == tableau_de_tokens[7] && tableau_de_tokens[7] != 2)
        win = playeur;
    else if (tableau_de_tokens[2] == tableau_de_tokens[5] && tableau_de_tokens[5] == tableau_de_tokens[8] && tableau_de_tokens[8] != 2)
        win = playeur;
    else if (tableau_de_tokens[0] == tableau_de_tokens[4] && tableau_de_tokens[4] == tableau_de_tokens[8] && tableau_de_tokens[8] != 2)
        win = playeur;
    else if (tableau_de_tokens[6] == tableau_de_tokens[4] && tableau_de_tokens[4] == tableau_de_tokens[2] && tableau_de_tokens[2] != 2)
        win = playeur;

    if (win != 2) {
        win_message(playeur);
        ctx.stop();
    }
}

void play_noughts_and_cross()
{
    bool             playeur_1 = false;
    std::vector<int> tableau_de_tokens{2, 2, 2, 2, 2, 2, 2, 2, 2};
    int              Cell_clicked = 33;
    auto             ctx          = p6::Context{{900, 900, "Noughts and Crosses"}}; // Create a context with a window
    ctx.update                    = [&]() {
        Draw_Grid(ctx);

        if (Cell_clicked != 33) {
            tableau_de_tokens[Cell_clicked] = playeur_1;
            Draw_tokens(ctx, tableau_de_tokens);
        }
        if (playeur_1 == false) {
            Draw_one_token(ctx, "img/raclette.png", ctx.mouse().x, ctx.mouse().y, 0.35);
        }
        else {
            Draw_one_token(ctx, "img/potato.png", ctx.mouse().x, ctx.mouse().y, 0.2);
        }
        win(ctx, !playeur_1, tableau_de_tokens);
    };

    ctx.mouse_pressed = [&](auto) {
        playeur_1    = !playeur_1;
        Cell_clicked = cell_clicked(ctx.mouse().x, ctx.mouse().y);
    };

    ctx.start();
}