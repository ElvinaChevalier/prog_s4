#include "play_guess_the_number.h"
#include <iostream>
#include "get_input_from_user.h"
#include "message.h"
#include "random.h"

int get_int_from_user()
{
    int number;
    while (!(std::cin >> number)) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "It is not a number.\n";
    }
    return number;
}

void play_guess_the_number()
{
    static constexpr int min             = 0;
    static constexpr int max             = 100;
    int                  number_to_guess = rand(min, max);
    bool                 guess           = false;

    while (!guess) {
        std::cout << "pick a number between 0 and 100" << std::endl;
        const auto number_user = get_input_from_user<int>();

        if (number_user < number_to_guess)
            std::cout << "it's more" << std::endl;
        else if (number_user > number_to_guess)
            std::cout << "it's less" << std::endl;
        else {
            win_message();
            guess = true;
        }
    }
}